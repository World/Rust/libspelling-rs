# libspelling-rs

The Rust bindings of [libspelling](https://gitlab.gnome.org/chergert/libspelling)

Website: <https://world.pages.gitlab.gnome.org/Rust/libspelling-rs>

## Documentation

- libspelling: <https://world.pages.gitlab.gnome.org/Rust/libspelling-rs/stable/latest/docs/libspelling>
- libspelling-sys: <https://world.pages.gitlab.gnome.org/Rust/libspelling-rs/stable/latest/docs/libspelling_sys>
