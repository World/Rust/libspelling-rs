#![cfg_attr(docsrs, feature(doc_cfg))]

// Re-export the -sys bindings
pub use ffi;
pub use gio;
pub use glib;
pub use gtk;

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("libspelling may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libspelling.");
            }
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;

pub use auto::*;
pub use functions::*;

pub mod builders;
pub mod prelude;

mod checker;
