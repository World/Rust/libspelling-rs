// Take a look at the license at the top of the repository in the LICENSE file.

use crate::{Checker, Provider};

impl Checker {
    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`Checker`] objects.
    ///
    /// This method returns an instance of [`CheckerBuilder`](crate::builders::CheckerBuilder) which can be used to create [`Checker`] objects.
    pub fn builder() -> CheckerBuilder {
        CheckerBuilder::new()
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`Checker`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct CheckerBuilder {
    builder: glib::object::ObjectBuilder<'static, Checker>,
}

impl CheckerBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn language(self, language: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("language", language.into()),
        }
    }

    pub fn provider(self, provider: &Provider) -> Self {
        Self {
            builder: self.builder.property("provider", provider.clone()),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`Checker`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Checker {
        self.builder.build()
    }
}
